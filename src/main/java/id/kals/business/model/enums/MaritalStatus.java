package id.kals.business.model.enums;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
public enum MaritalStatus {
    M("Married"),S("Single"),D("Divorced"),W("Widowed"),O("OTHER"),E("EMPTY");

    private final String maritalStatus;

    MaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getMaritalStatus() {
        return this.maritalStatus;
    }
}
