package id.kals.business.model.enums;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
public enum Gender {
    M("Male"),F("Female"),O("Other");

    private final String gender;


    Gender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return this.gender;
    }
}
