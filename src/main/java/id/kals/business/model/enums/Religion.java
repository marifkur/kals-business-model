package id.kals.business.model.enums;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
public enum Religion {
    ISLAM("AGAMA ISLAM"),HINDU("AGAMA HINDU"),
    BUDHA("AGAMA BUDHA"),
    KRISTEN("AGAMA KRISTEN"),
    KATOLIK ("AGAMA KATOLIK"),
    KONGHUCU ("KONGHUCU"),
    OTHER("OTHER");

    private final String religion;

    Religion(String religion) {
        this.religion = religion;
    }

    public String getReligion() {
        return religion;
    }
}
