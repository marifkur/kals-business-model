package id.kals.business.model.enums;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
public enum Status {
    DISABLE(0),ACTIVE(1),DELETE(2),REQUEST(3),WAITING(4),APPROVE(5),
    REJECT(6),BLOCK(7),FINISH(8),CANCEL(9),UNVERIFIED(10),VERIFIED(11);

    private final int value;

    Status(int value) {this.value = value;

    }
    public int getValue() {return value;}

    public static Status fromValue(int value) {
        for (Status aip: values()) {
            if (aip.getValue() == value) {
                return aip;
            }
        }
        return null;
    }
}
