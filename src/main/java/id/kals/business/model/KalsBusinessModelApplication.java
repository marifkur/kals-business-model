package id.kals.business.model;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(
		scanBasePackages = {
				"id.kals.business.model",

		}
)

@ComponentScan(
		basePackages = {
				"id.kals.business.model.service",
				"id.kals.business.model.enums",
				"id.kals.business.model.exceptions",
				"id.kals.business.model.model",
				"id.kals.business.model.utils",

		}
)

@EnableJpaRepositories(
		basePackages = {
				"id.kals.business.model.repository",


		}
)

@EntityScan(
		basePackages = {
				"id.kals.business.model",

		}
)
public class KalsBusinessModelApplication {

	public static void main(String[] args) {
		SpringApplication.run(KalsBusinessModelApplication.class, args);
	}

}
