package id.kals.business.model.utils;

import id.kals.business.model.entity.Payment;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.Nullable;

import javax.persistence.criteria.*;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
public class BaseSpecification<T> implements Specification<T> {

    private SearchCriteria criteria;

    public BaseSpecification(SearchCriteria criteria) {
        this.criteria = criteria;
    }

    @Nullable
    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

        if (criteria.getOperation().equalsIgnoreCase(">")) {
            return criteriaBuilder.greaterThanOrEqualTo(
                    root.get(criteria.getKey()).as(Date.class), (Date) criteria.getValue());
        } else if (criteria.getOperation().equalsIgnoreCase("<")) {
            return criteriaBuilder.lessThanOrEqualTo(
                    root.get(criteria.getKey()).as(Date.class), (Date) criteria.getValue());
        } else if (criteria.getOperation().equalsIgnoreCase(":")) {
            if (criteria.getKey().contains(".")) {
                String[] key = criteria.getKey().split("\\.");
                return criteriaBuilder.like(
                        root.get(key[0]).get(key[1]), "%" + criteria.getValue() + "%");
            } else if (root.get(criteria.getKey()).getJavaType() == String.class) {
                return criteriaBuilder.like(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else {
                return criteriaBuilder.equal(root.get(criteria.getKey()), criteria.getValue());
            }
        } else if (criteria.getOperation().equalsIgnoreCase("~")) {

            if (criteria.getKey().contains("\\.")) {
                Root<Payment> paymentRoot = query.from(Payment.class);
                String[] key = criteria.getKey().split("\\.");
                Path<LocalDateTime> dateTimePath = paymentRoot.<LocalDateTime>get(key[0]);
                Expression<Integer> dbYear = criteriaBuilder.function("year", Integer.class, dateTimePath);
                Expression<Integer> dbMonth = criteriaBuilder.function("month", Integer.class, dateTimePath);
                if (key[1].equals("month")) {
                    return criteriaBuilder.equal(
                            dbMonth, criteria.getValue());
                } else if (key[1].equals("month-year")) {
                    if (criteria.getValue().toString().contains("\\.")) {
                        String[] value = criteria.getValue().toString().split("\\.");

                        return criteriaBuilder.and(
                                criteriaBuilder.equal(dbMonth, value[0]),
                                criteriaBuilder.equal(dbYear, value[1]));
                    }
                } else {
                    return criteriaBuilder.equal(
                            dbYear, criteria.getValue());
                }
            }
        }
        return null;
    }
}
