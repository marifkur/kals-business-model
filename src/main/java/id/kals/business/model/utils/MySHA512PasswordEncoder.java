package id.kals.business.model.utils;

import org.apache.commons.codec.digest.Crypt;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
public class MySHA512PasswordEncoder implements PasswordEncoder {

    private static final String SHA_512_KEY = "$6$";

    @Override
    public String encode(CharSequence charSequence) {
        return hashPass(charSequence,"");
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        return matchPass(charSequence, s);
    }

    public String hashPass(CharSequence charSequence, String salt) {

        MyUtils utils = new MyUtils();

        String keyMd5 = "";

        if (salt.trim().isEmpty()){
            keyMd5 = SHA_512_KEY + utils.generateRandom(16) + "$";
        }else {
            keyMd5 = salt;
        }

        String crypt = Crypt.crypt(charSequence.toString(), keyMd5);

        return crypt;
    }

    public Boolean matchPass(CharSequence charSequence, String s) {
        boolean match = false;

        String[] keyChar = s.split("\\$");

        String key512 = SHA_512_KEY + keyChar[2] + "$"; // $6$Vlj38gSaeYLrhwBr$
        String sha512Match = Crypt.crypt(charSequence.toString(), key512);
        if (s.equals(sha512Match)) {
            match = true;
        }

        return match;
    }

}
