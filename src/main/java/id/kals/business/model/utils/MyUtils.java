package id.kals.business.model.utils;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
public class MyUtils {

    private static final String ALPHA_STRING = "ABCDEFGHJKMNPQRSTUVWXYZ";
    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHJKMNPQRSTUVWXYZabcdefghjkmnopqrstuvwxyz123456789";
    private static final String ALPHA_NUMERIC = "0123456789";

    public String generateRandom(int count) {
        StringBuilder builder = new StringBuilder();
        int characterFirst = (int)(Math.random()*ALPHA_STRING.length());
        builder.append(ALPHA_NUMERIC_STRING.charAt(characterFirst));
        count--;

        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    public String generateNumericRandom(int count) {
        StringBuilder builder = new StringBuilder();
        int characterFirst = (int)(Math.random()*ALPHA_NUMERIC.length());
        builder.append(ALPHA_NUMERIC.charAt(characterFirst));
        count--;

        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC.length());
            builder.append(ALPHA_NUMERIC.charAt(character));
        }
        return builder.toString();
    }

}
