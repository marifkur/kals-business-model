package id.kals.business.model.utils;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
public interface BaseService<T> {
    T save(T entity);

    Boolean delete(T entity);

    T findById(String id);

    Page<T> findAllPage(Pageable pageable);
}
