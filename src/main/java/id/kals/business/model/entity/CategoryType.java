package id.kals.business.model.entity;

import id.kals.business.model.enums.Status;
import id.kals.business.model.model.BaseIdEntity;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Entity
@Table(name = "category_type")
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class CategoryType extends BaseIdEntity {

    @NotNull(message = "category id cannot be null")
    @OneToOne
    @JoinColumn(name = "category_id", nullable = false, foreignKey = @ForeignKey(name = "FK_CategoryToCategoryType"))
    private Category category;

    @NotNull(message = "type id cannot be null")
    @OneToOne
    @JoinColumn(name = "type_id", nullable = false, foreignKey = @ForeignKey(name = "FK_TypeToCategoryType"))
    private Type type;

    @Enumerated(EnumType.ORDINAL)
    @Column(columnDefinition = "status",  length = 1)
    @ColumnDefault(value = "")
    private Status status;
}
