package id.kals.business.model.entity;

import id.kals.business.model.enums.Status;
import id.kals.business.model.model.BaseIdEntity;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Entity
@Table(name = "personal_user_type")
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class PersonalUserType extends BaseIdEntity {

    @ManyToOne
    @JoinColumn(name = "personal_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PersonalToPersonalUserType"))
    private Personal personal;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false, foreignKey = @ForeignKey(name = "FK_UserToPersonalUserType"))
    private Users user;

    @ManyToOne
    @JoinColumn(name = "category_type_id", nullable = false, foreignKey = @ForeignKey(name = "FK_CategoryTypeToPersonalUserType"))
    private CategoryType categoryType;

    @Enumerated(EnumType.ORDINAL)
    @Column(columnDefinition = "status",  length = 1)
    @ColumnDefault(value = "")
    private Status status;
}
