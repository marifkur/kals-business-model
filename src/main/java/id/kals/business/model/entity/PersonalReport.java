package id.kals.business.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import id.kals.business.model.enums.Status;
import id.kals.business.model.model.BaseIdEntity;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Entity
@Table(name = "personal_report")
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class PersonalReport extends BaseIdEntity {

    @ManyToOne
    @JoinColumn(name = "personal_visitor_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PersonalToPersonalReportVisit"))
    private Personal personalVisitor;

    @ManyToOne
    @JoinColumn(name = "personal_employee_id", foreignKey = @ForeignKey(name = "FK_PersonalToPersonalReportEmployee"))
    private Personal personalEmployee;

    @ManyToOne
    @JoinColumn(name = "personal_respondent_id", foreignKey = @ForeignKey(name = "FK_PersonalToPersonalReportRespondent"))
    private Personal personalRespondent;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "start_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime startDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "end_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime endDate;

    @Column(name = "report", nullable = false)
    private String report;

    @Column(name = "response", nullable = false)
    private String response;

    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.ORDINAL)
    @Column(columnDefinition = "status", length = 1)
    @ColumnDefault(value = "")
    private Status status;

    @JsonIgnore
    @OneToMany(mappedBy = "personalReport")
    private List<PersonalReportDetail> personalReportDetailList;
}
