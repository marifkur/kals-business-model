package id.kals.business.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import id.kals.business.model.enums.Status;
import id.kals.business.model.model.BaseIdEntity;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Entity
@Table(name = "payment")
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class Payment extends BaseIdEntity {

    @ManyToOne
    @JoinColumn(name = "personal_report_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PersonalReportToPayment"))
    private PersonalReport personalReport;

    @ManyToOne
    @JoinColumn(name = "payment_method_id", nullable = false, foreignKey = @ForeignKey(name = "FK_PaymentMethodToPayment"))
    private PaymentMethod paymentMethod;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "payment_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime paymentDate;

    @NotNull(message = "total can't be null")
    @Column(name = "total", nullable = false)
    private Double total;

    @NotNull(message = "response_final can't be null")
    @Column(name = "response_final", nullable = false)
    private String responseFinal;

    @NotNull(message = "description can't be null")
    @Column(name = "description", nullable = false)
    private String description;

    @Enumerated(EnumType.ORDINAL)
    @Column(columnDefinition = "status",  length = 1)
    @ColumnDefault(value = "")
    private Status status;
}
