package id.kals.business.model.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.kals.business.model.enums.Gender;
import id.kals.business.model.enums.MaritalStatus;
import id.kals.business.model.enums.Religion;
import id.kals.business.model.enums.Status;
import id.kals.business.model.model.BaseIdEntity;
import id.kals.business.model.utils.JsonDateDeserializer;
import id.kals.business.model.utils.JsonDateSerializer;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.Date;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Entity
@Table(name = "personal")
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class Personal extends BaseIdEntity {

    @Column(name = "first_name", length = 50, nullable = false)
    private String firstName;

    @Column(name = "last_name", length = 50, nullable = false)
    private String lastName;

    @Column(name = "nickname", length = 50, nullable = false)
    private String nickName;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "gender",  length = 1)
    @ColumnDefault(value = "")
    private Gender gender;

    @Enumerated(EnumType.STRING)
    @Column(name = "marital_status",  length = 1)
    @ColumnDefault(value = "")
    private MaritalStatus maritalStatus;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "religion",  length = 20)
    @ColumnDefault(value = "")
    private Religion religion;

    @JsonDeserialize(using = JsonDateDeserializer.class)
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    @Column(name = "birth_date", nullable = false)
    @ColumnDefault(value = "")
    private Date birthDate;

    @Column(name = "place_of_birth", nullable = false)
    private String placeOfBirth;

    @Column(name = "mother_name", length = 50, nullable = false)
    @ColumnDefault(value = "")
    private String motherName;

    @Enumerated(EnumType.ORDINAL)
    @Column(columnDefinition = "status",  length = 1)
    @ColumnDefault(value = "")
    private Status status;
}
