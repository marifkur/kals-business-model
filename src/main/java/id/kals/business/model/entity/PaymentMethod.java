package id.kals.business.model.entity;

import id.kals.business.model.enums.Status;
import id.kals.business.model.model.BaseIdEntity;
import lombok.*;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Entity
@Table(name = "payment_method")
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class PaymentMethod extends BaseIdEntity {

    @NotNull(message = "type can't be null")
    @Column(name = "type", length = 20, nullable = false)
    private String type;

    @NotNull(message = "name can't be null")
    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @NotNull(message = "value can't be null")
    @Column(name = "value", nullable = false)
    private Double value;

    @Enumerated(EnumType.ORDINAL)
    @Column(columnDefinition = "status",  length = 1)
    @ColumnDefault(value = "")
    private Status status;
}
