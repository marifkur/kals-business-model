package id.kals.business.model.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Getter
@Setter
public class PersonalReportDetailModel {

    private String type;

    private String name;

    private Double cost;
}
