package id.kals.business.model.model;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Getter
@Setter
public class Source {
    private String pointer;
    private String parameter;
}
