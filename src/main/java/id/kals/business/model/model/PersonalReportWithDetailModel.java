package id.kals.business.model.model;

import id.kals.business.model.entity.PersonalReport;
import id.kals.business.model.entity.PersonalReportDetail;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Getter
@Setter
public class PersonalReportWithDetailModel {
    private PersonalReport personalReport;

    private List<PersonalReportDetail> personalReportDetailList;
}
