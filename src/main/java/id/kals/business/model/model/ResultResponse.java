package id.kals.business.model.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResultResponse {

    private String code;

    private String contentId;

    private String message;

    private String description;

}
