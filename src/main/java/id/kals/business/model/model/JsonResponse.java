package id.kals.business.model.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Getter
@Setter
public class JsonResponse<T> {

    List<Error> errors = new ArrayList();

    T data;

    Map<String,?> dictionaries = new HashMap();

}
