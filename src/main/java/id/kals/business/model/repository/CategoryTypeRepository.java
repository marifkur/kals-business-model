package id.kals.business.model.repository;

import id.kals.business.model.entity.Category;
import id.kals.business.model.entity.CategoryType;
import id.kals.business.model.entity.Type;
import id.kals.business.model.enums.Status;
import id.kals.business.model.utils.BaseRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Repository
public interface CategoryTypeRepository extends BaseRepository<CategoryType, String>, JpaSpecificationExecutor<CategoryType> {
    CategoryType findByIdAndStatus(String id, Status status);

    CategoryType findByCategoryAndStatus(Category category, Status status);

    CategoryType findByTypeAndStatus(Type category, Status status);
}
