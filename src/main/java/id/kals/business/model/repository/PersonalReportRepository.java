package id.kals.business.model.repository;

import id.kals.business.model.entity.Personal;
import id.kals.business.model.entity.PersonalReport;
import id.kals.business.model.enums.Status;
import id.kals.business.model.utils.BaseRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Repository
public interface PersonalReportRepository extends BaseRepository<PersonalReport, String>, JpaSpecificationExecutor<PersonalReport> {
    PersonalReport findByIdAndStatus(String id, Status status);

    List<PersonalReport> findByPersonalEmployeeAndStatus(Personal personal, Status status);

    List<PersonalReport> findByPersonalVisitorAndStatus(Personal personal, Status status);

    List<PersonalReport> findByPersonalRespondentAndStatus(Personal personal, Status status);

}
