package id.kals.business.model.repository;

import id.kals.business.model.entity.PaymentMethod;
import id.kals.business.model.enums.Status;
import id.kals.business.model.utils.BaseRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Repository
public interface PaymentMethodRepository extends BaseRepository<PaymentMethod, String>, JpaSpecificationExecutor<PaymentMethod> {
    PaymentMethod findByIdAndStatus(String id, Status status);

    List<PaymentMethod> findByTypeAndStatus(String type, Status status);
}
