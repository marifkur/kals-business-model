package id.kals.business.model.repository;

import id.kals.business.model.entity.Type;
import id.kals.business.model.enums.Status;
import id.kals.business.model.utils.BaseRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Repository
public interface TypeRepository extends BaseRepository<Type, String>, JpaSpecificationExecutor<Type> {
    Type findByIdAndStatus(String id, Status status);

    List<Type> findByNameAndStatus(String name, Status status);

}
