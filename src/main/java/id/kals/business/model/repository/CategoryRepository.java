package id.kals.business.model.repository;

import id.kals.business.model.entity.Category;
import id.kals.business.model.enums.Status;
import id.kals.business.model.utils.BaseRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Repository
public interface CategoryRepository extends BaseRepository<Category, String>, JpaSpecificationExecutor<Category> {
    Category findByIdAndStatus(String id, Status status);

    List<Category> findByNameAndStatus(String name, Status status);
}
