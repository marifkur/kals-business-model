package id.kals.business.model.repository;

import id.kals.business.model.entity.Personal;
import id.kals.business.model.enums.Status;
import id.kals.business.model.utils.BaseRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Repository
public interface PersonalRepository extends BaseRepository<Personal, String>, JpaSpecificationExecutor<Personal> {
    Personal findByIdAndStatus(String id, Status status);

}
