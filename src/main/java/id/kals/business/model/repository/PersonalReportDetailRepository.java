package id.kals.business.model.repository;

import id.kals.business.model.entity.PersonalReport;
import id.kals.business.model.entity.PersonalReportDetail;
import id.kals.business.model.enums.Status;
import id.kals.business.model.utils.BaseRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Repository
public interface PersonalReportDetailRepository extends BaseRepository<PersonalReportDetail, String>, JpaSpecificationExecutor<PersonalReportDetail> {
    PersonalReportDetail findByIdAndStatus(String id, Status status);

    List<PersonalReportDetail> findByPersonalReportAndStatus(PersonalReport personalReport, Status status);

}
