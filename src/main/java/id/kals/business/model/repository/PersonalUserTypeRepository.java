package id.kals.business.model.repository;

import id.kals.business.model.entity.Personal;
import id.kals.business.model.entity.PersonalUserType;
import id.kals.business.model.entity.Users;
import id.kals.business.model.enums.Status;
import id.kals.business.model.utils.BaseRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Repository
public interface PersonalUserTypeRepository extends BaseRepository<PersonalUserType, String>, JpaSpecificationExecutor<PersonalUserType> {
    PersonalUserType findByIdAndStatus(String id, Status status);

    List<PersonalUserType> findByPersonalAndStatus(Personal personal, Status status);

    List<PersonalUserType> findByUserAndStatus(Users users, Status status);

}
