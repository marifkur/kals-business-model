package id.kals.business.model.repository;

import id.kals.business.model.entity.Users;
import id.kals.business.model.enums.Status;
import id.kals.business.model.utils.BaseRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Repository
public interface UsersRepository extends BaseRepository<Users, String>, JpaSpecificationExecutor<Users> {
    Users findByIdAndStatus(String id, Status status);
    Users findByUsernameAndStatus(String username, Status status);

}
