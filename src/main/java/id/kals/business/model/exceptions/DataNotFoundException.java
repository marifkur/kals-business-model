package id.kals.business.model.exceptions;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
public class DataNotFoundException extends RuntimeException {
    public DataNotFoundException(){super();}
    public DataNotFoundException(String message){super(message);}
}
