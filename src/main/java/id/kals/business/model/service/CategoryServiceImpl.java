package id.kals.business.model.service;

import id.kals.business.model.entity.Category;
import id.kals.business.model.enums.Status;
import id.kals.business.model.repository.CategoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Slf4j
@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Category findByIdAndStatus(String id, Status status) {
        return categoryRepository.findByIdAndStatus(id, status);
    }

    @Override
    public List<Category> findByNameAndStatus(String name, Status status) {
        return categoryRepository.findByNameAndStatus(name, status);
    }

    @Override
    public Page<Category> findAllSpecification(Specification<Category> categorySpecification, Pageable pageable) {
        return categoryRepository.findAll(categorySpecification, pageable);
    }

    @Override
    public Category save(Category entity) {
        Category response = new Category();

        if (!StringUtils.isEmpty(entity.getId())) {
            Category validCategory = categoryRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validCategory != null) {
                BeanUtils.copyProperties(entity, validCategory);
                validCategory.setStatus(entity.getStatus());
                validCategory.setModifiedAt(entity.getModifiedAt());
                validCategory.setModifiedBy(entity.getModifiedBy());
                validCategory.setModifiedIp(entity.getModifiedIp());

                Category updateCategory = categoryRepository.save(validCategory);
                response = updateCategory;
            }
        } else
            response = categoryRepository.save(entity);

        return response;
    }

    @Override
    public Boolean delete(Category entity) {
        Boolean response = false;

        if (!StringUtils.isEmpty(entity.getId())) {
            Category validCategory = categoryRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validCategory != null) {
                validCategory.setStatus(Status.DELETE);
                categoryRepository.save(validCategory);
                response = true;
            }
        }
        return response;
    }

    @Override
    public Category findById(String id) {
        return categoryRepository.findById(id);
    }

    @Override
    public Page<Category> findAllPage(Pageable pageable) {
        return categoryRepository.findAll(pageable);
    }
}
