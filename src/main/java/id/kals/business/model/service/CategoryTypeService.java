package id.kals.business.model.service;

import id.kals.business.model.entity.Category;
import id.kals.business.model.entity.CategoryType;
import id.kals.business.model.entity.Type;
import id.kals.business.model.enums.Status;
import id.kals.business.model.utils.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
public interface CategoryTypeService extends BaseService<CategoryType> {

    CategoryType findByIdAndStatus(String id, Status status);

    CategoryType findByCategoryAndStatus(Category category, Status status);

    CategoryType findByTypeAndStatus(Type type, Status status);

    Page<CategoryType> findAllSpecification(Specification<CategoryType> categoryTypeSpecification, Pageable pageable);

}