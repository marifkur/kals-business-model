package id.kals.business.model.service;

import id.kals.business.model.entity.PersonalReport;
import id.kals.business.model.entity.PersonalReportDetail;
import id.kals.business.model.enums.Status;
import id.kals.business.model.repository.PersonalReportDetailRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Slf4j
@Service
public class PersonalReportDetailServiceImpl implements PersonalReportDetailService {
    @Autowired
    private PersonalReportDetailRepository personalReportDetailRepository;

    @Override
    public PersonalReportDetail findByIdAndStatus(String id, Status status) {
        return personalReportDetailRepository.findByIdAndStatus(id, status);
    }

    @Override
    public List<PersonalReportDetail> findByPersonalReportAndStatus(PersonalReport personalReport, Status status) {
        return personalReportDetailRepository.findByPersonalReportAndStatus(personalReport, status);
    }

    @Override
    public Page<PersonalReportDetail> findAllSpecification(Specification<PersonalReportDetail> personalReportDetailSpecification, Pageable pageable) {
        return personalReportDetailRepository.findAll(personalReportDetailSpecification, pageable);
    }

    @Override
    public PersonalReportDetail save(PersonalReportDetail entity) {
        PersonalReportDetail response = new PersonalReportDetail();

        if (!StringUtils.isEmpty(entity.getId())) {
            PersonalReportDetail validPersonalReportDetail = personalReportDetailRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validPersonalReportDetail != null) {
                BeanUtils.copyProperties(entity, validPersonalReportDetail);
                validPersonalReportDetail.setStatus(entity.getStatus());
                validPersonalReportDetail.setPersonalReport(entity.getPersonalReport());
                validPersonalReportDetail.setModifiedAt(entity.getModifiedAt());
                validPersonalReportDetail.setModifiedBy(entity.getModifiedBy());
                validPersonalReportDetail.setModifiedIp(entity.getModifiedIp());

                PersonalReportDetail updatePersonalReportDetail = personalReportDetailRepository.save(validPersonalReportDetail);
                response = updatePersonalReportDetail;
            }
        } else
            response = personalReportDetailRepository.save(entity);

        return response;
    }

    @Override
    public Boolean delete(PersonalReportDetail entity) {
        Boolean response = false;

        if (!StringUtils.isEmpty(entity.getId())) {
            PersonalReportDetail validPersonalReportDetail = personalReportDetailRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validPersonalReportDetail != null) {
                validPersonalReportDetail.setStatus(Status.DELETE);
                personalReportDetailRepository.save(validPersonalReportDetail);
                response = true;
            }
        }
        return response;
    }

    @Override
    public PersonalReportDetail findById(String id) {
        return personalReportDetailRepository.findById(id);
    }

    @Override
    public Page<PersonalReportDetail> findAllPage(Pageable pageable) {
        return personalReportDetailRepository.findAll(pageable);
    }
}
