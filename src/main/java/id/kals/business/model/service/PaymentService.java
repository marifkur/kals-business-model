package id.kals.business.model.service;

import id.kals.business.model.entity.Payment;
import id.kals.business.model.entity.PersonalReport;
import id.kals.business.model.enums.Status;
import id.kals.business.model.utils.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
public interface PaymentService extends BaseService<Payment> {

    Payment findByIdAndStatus(String id, Status status);

    List<Payment> findByPersonalReportAndStatus(PersonalReport personalReport, Status status);

    Page<Payment> findAllSpecification(Specification<Payment> paymentSpecification, Pageable pageable);

}