package id.kals.business.model.service;

import id.kals.business.model.entity.Type;
import id.kals.business.model.enums.Status;
import id.kals.business.model.repository.TypeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Service
@Slf4j
public class TypeServiceImpl implements TypeService {
    @Autowired
    private TypeRepository typeRepository;

    @Override
    public Type findByIdAndStatus(String id, Status status) {
        return typeRepository.findByIdAndStatus(id, status);
    }

    @Override
    public List<Type> findByNameAndStatus(String name, Status status) {
        return typeRepository.findByNameAndStatus(name, status);
    }

    @Override
    public Page<Type> findAllSpecification(Specification<Type> typeSpecification, Pageable pageable) {
        return typeRepository.findAll(typeSpecification, pageable);
    }

    @Override
    public Type save(Type entity) {
        Type response = new Type();

        if (!StringUtils.isEmpty(entity.getId())) {
            Type validType = typeRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validType != null) {
                BeanUtils.copyProperties(entity, validType);
                validType.setStatus(entity.getStatus());
                validType.setModifiedAt(entity.getModifiedAt());
                validType.setModifiedBy(entity.getModifiedBy());
                validType.setModifiedIp(entity.getModifiedIp());

                Type updateType = typeRepository.save(validType);
                response = updateType;
            }
        } else
            response = typeRepository.save(entity);

        return response;
    }

    @Override
    public Boolean delete(Type entity) {
        Boolean response = false;

        if (!StringUtils.isEmpty(entity.getId())) {
            Type validType = typeRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validType != null) {
                validType.setStatus(Status.DELETE);
                typeRepository.save(validType);
                response = true;
            }
        }
        return response;
    }

    @Override
    public Type findById(String id) {
        return typeRepository.findById(id);
    }

    @Override
    public Page<Type> findAllPage(Pageable pageable) {
        return typeRepository.findAll(pageable);
    }
}
