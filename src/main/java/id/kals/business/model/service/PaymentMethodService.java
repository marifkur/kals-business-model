package id.kals.business.model.service;

import id.kals.business.model.entity.PaymentMethod;
import id.kals.business.model.entity.Type;
import id.kals.business.model.enums.Status;
import id.kals.business.model.utils.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
public interface PaymentMethodService extends BaseService<PaymentMethod> {

    PaymentMethod findByIdAndStatus(String id, Status status);

    List<PaymentMethod> findByTypeAndStatus(String type, Status status);

    Page<PaymentMethod> findAllSpecification(Specification<PaymentMethod> paymentMethodSpecification, Pageable pageable);

}