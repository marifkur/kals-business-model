package id.kals.business.model.service;

import id.kals.business.model.entity.PaymentMethod;
import id.kals.business.model.enums.Status;
import id.kals.business.model.repository.PaymentMethodRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Slf4j
@Service
public class PaymentMethodServiceImpl implements PaymentMethodService {
    @Autowired
    private PaymentMethodRepository paymentMethodRepository;

    @Override
    public PaymentMethod findByIdAndStatus(String id, Status status) {
        return paymentMethodRepository.findByIdAndStatus(id, status);
    }

    @Override
    public List<PaymentMethod> findByTypeAndStatus(String type, Status status) {
        return paymentMethodRepository.findByTypeAndStatus(type, status);
    }

    @Override
    public Page<PaymentMethod> findAllSpecification(Specification<PaymentMethod> paymentMethodSpecification, Pageable pageable) {
        return paymentMethodRepository.findAll(paymentMethodSpecification, pageable);
    }

    @Override
    public PaymentMethod save(PaymentMethod entity) {
        PaymentMethod response = new PaymentMethod();

        if (!StringUtils.isEmpty(entity.getId())) {
            PaymentMethod validPaymentMethod = paymentMethodRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validPaymentMethod != null) {
                BeanUtils.copyProperties(entity, validPaymentMethod);
                validPaymentMethod.setStatus(entity.getStatus());
                validPaymentMethod.setModifiedAt(entity.getModifiedAt());
                validPaymentMethod.setModifiedBy(entity.getModifiedBy());
                validPaymentMethod.setModifiedIp(entity.getModifiedIp());

                PaymentMethod updatePaymentMethod = paymentMethodRepository.save(validPaymentMethod);
                response = updatePaymentMethod;
            }
        } else
            response = paymentMethodRepository.save(entity);

        return response;
    }

    @Override
    public Boolean delete(PaymentMethod entity) {
        Boolean response = false;

        if (!StringUtils.isEmpty(entity.getId())) {
            PaymentMethod validPaymentMethod = paymentMethodRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validPaymentMethod != null) {
                validPaymentMethod.setStatus(Status.DELETE);
                paymentMethodRepository.save(validPaymentMethod);
                response = true;
            }
        }
        return response;
    }

    @Override
    public PaymentMethod findById(String id) {
        return paymentMethodRepository.findById(id);
    }

    @Override
    public Page<PaymentMethod> findAllPage(Pageable pageable) {
        return paymentMethodRepository.findAll(pageable);
    }
}
