package id.kals.business.model.service;

import id.kals.business.model.entity.Personal;
import id.kals.business.model.entity.PersonalUserType;
import id.kals.business.model.entity.Users;
import id.kals.business.model.enums.Status;
import id.kals.business.model.repository.PersonalUserTypeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Slf4j
@Service
public class PersonalUserTypeServiceImpl implements PersonalUserTypeService {

    @Autowired
    private PersonalUserTypeRepository personalUserTypeRepository;

    @Override
    public PersonalUserType findByIdAndStatus(String id, Status status) {
        return personalUserTypeRepository.findByIdAndStatus(id, status);
    }

    @Override
    public List<PersonalUserType> findByPersonalAndStatus(Personal personal, Status status) {
        return personalUserTypeRepository.findByPersonalAndStatus(personal, status);
    }

    @Override
    public List<PersonalUserType> findByUserAndStatus(Users users, Status status) {
        return personalUserTypeRepository.findByUserAndStatus(users, status);
    }

    @Override
    public Page<PersonalUserType> findAllSpecification(Specification<PersonalUserType> personalUserTypeSpecification, Pageable pageable) {
        return personalUserTypeRepository.findAll(personalUserTypeSpecification, pageable);
    }

    @Override
    public PersonalUserType save(PersonalUserType entity) {
        PersonalUserType response = new PersonalUserType();

        if (!StringUtils.isEmpty(entity.getId())) {
            PersonalUserType validPersonalUserType = personalUserTypeRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validPersonalUserType != null) {
                BeanUtils.copyProperties(entity, validPersonalUserType);
                validPersonalUserType.setStatus(entity.getStatus());
                validPersonalUserType.setModifiedAt(entity.getModifiedAt());
                validPersonalUserType.setModifiedBy(entity.getModifiedBy());
                validPersonalUserType.setModifiedIp(entity.getModifiedIp());
                validPersonalUserType.setPersonal(entity.getPersonal());
                validPersonalUserType.setUser(entity.getUser());
                validPersonalUserType.setCategoryType(entity.getCategoryType());
                PersonalUserType updatePersonalUserType = personalUserTypeRepository.save(validPersonalUserType);
                response = updatePersonalUserType;
            }
        } else
            response = personalUserTypeRepository.save(entity);

        return response;
    }

    @Override
    public Boolean delete(PersonalUserType entity) {
        Boolean response = false;

        if (!StringUtils.isEmpty(entity.getId())) {
            PersonalUserType validPersonalUserType = personalUserTypeRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validPersonalUserType != null) {
                validPersonalUserType.setStatus(Status.DELETE);
                personalUserTypeRepository.save(validPersonalUserType);
                response = true;
            }
        }
        return response;
    }

    @Override
    public PersonalUserType findById(String id) {
        return personalUserTypeRepository.findById(id);
    }

    @Override
    public Page<PersonalUserType> findAllPage(Pageable pageable) {
        return personalUserTypeRepository.findAll(pageable);
    }
}
