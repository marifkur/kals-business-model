package id.kals.business.model.service;

import id.kals.business.model.entity.Personal;
import id.kals.business.model.entity.PersonalReport;
import id.kals.business.model.enums.Status;
import id.kals.business.model.model.PersonalReportDetailModel;
import id.kals.business.model.model.PersonalReportWithDetailModel;
import id.kals.business.model.utils.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
public interface PersonalReportService extends BaseService<PersonalReport> {

    PersonalReport findByIdAndStatus(String id, Status status);

    List<PersonalReport> findByPersonalEmployeeAndStatus(Personal personal, Status status);

    List<PersonalReport> findByPersonalVisitorAndStatus(Personal personal, Status status);

    List<PersonalReport> findByPersonalRespondentAndStatus(Personal personal, Status status);

    Page<PersonalReport> findAllSpecification(Specification<PersonalReport> personalReportSpecification, Pageable pageable);

    PersonalReportWithDetailModel saveWithDetail(PersonalReport personalReport, List<PersonalReportDetailModel> personalReportDetailModels);
}