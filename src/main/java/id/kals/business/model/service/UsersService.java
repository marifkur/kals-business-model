package id.kals.business.model.service;

import id.kals.business.model.entity.CategoryType;
import id.kals.business.model.entity.Personal;
import id.kals.business.model.entity.Users;
import id.kals.business.model.enums.Status;
import id.kals.business.model.model.ResultResponse;
import id.kals.business.model.utils.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
public interface UsersService extends BaseService<Users> {

    Users findByIdAndStatus(String id, Status status);

    Users findByUsernameAndStatus(String username, Status status);

    Page<Users> findAllSpecification(Specification<Users> usersSpecification, Pageable pageable);

    ResultResponse deleteUserRegistered(String username);

    ResultResponse registerUser(Users users, Personal personal, CategoryType categoryType);
}
