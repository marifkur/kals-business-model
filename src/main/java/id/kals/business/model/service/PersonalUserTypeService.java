package id.kals.business.model.service;

import id.kals.business.model.entity.Personal;
import id.kals.business.model.entity.PersonalUserType;
import id.kals.business.model.entity.Type;
import id.kals.business.model.entity.Users;
import id.kals.business.model.enums.Status;
import id.kals.business.model.utils.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
public interface PersonalUserTypeService extends BaseService<PersonalUserType> {

    PersonalUserType findByIdAndStatus(String id, Status status);

    List<PersonalUserType> findByPersonalAndStatus(Personal personal, Status status);

    List<PersonalUserType> findByUserAndStatus(Users users, Status status);

    Page<PersonalUserType> findAllSpecification(Specification<PersonalUserType> personalUserTypeSpecification, Pageable pageable);

}