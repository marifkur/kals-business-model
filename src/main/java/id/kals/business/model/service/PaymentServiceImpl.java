package id.kals.business.model.service;

import id.kals.business.model.entity.Payment;
import id.kals.business.model.entity.PersonalReport;
import id.kals.business.model.enums.Status;
import id.kals.business.model.repository.PaymentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Slf4j
@Service
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public Payment findByIdAndStatus(String id, Status status) {
        return paymentRepository.findByIdAndStatus(id, status);
    }

    @Override
    public List<Payment> findByPersonalReportAndStatus(PersonalReport personalReport, Status status) {
        return paymentRepository.findByPersonalReportAndStatus(personalReport, status);
    }

    @Override
    public Page<Payment> findAllSpecification(Specification<Payment> paymentSpecification, Pageable pageable) {
        return paymentRepository.findAll(paymentSpecification, pageable);
    }

    @Override
    public Payment save(Payment entity) {
        Payment response = new Payment();

        if (!StringUtils.isEmpty(entity.getId())) {
            Payment validPayment = paymentRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validPayment != null) {
                BeanUtils.copyProperties(entity, validPayment);
                validPayment.setStatus(entity.getStatus());
                validPayment.setPaymentDate(entity.getPaymentDate());
                validPayment.setPaymentMethod(entity.getPaymentMethod());
                validPayment.setPersonalReport(entity.getPersonalReport());
                validPayment.setTotal(entity.getTotal());
                validPayment.setModifiedAt(entity.getModifiedAt());
                validPayment.setModifiedBy(entity.getModifiedBy());
                validPayment.setModifiedIp(entity.getModifiedIp());

                Payment updatePayment = paymentRepository.save(validPayment);
                response = updatePayment;
            }
        } else
            response = paymentRepository.save(entity);

        return response;
    }

    @Override
    public Boolean delete(Payment entity) {
        Boolean response = false;

        if (!StringUtils.isEmpty(entity.getId())) {
            Payment validPayment = paymentRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validPayment != null) {
                validPayment.setStatus(Status.DELETE);
                paymentRepository.save(validPayment);
                response = true;
            }
        }
        return response;
    }

    @Override
    public Payment findById(String id) {
        return null;
    }

    @Override
    public Page<Payment> findAllPage(Pageable pageable) {
        return null;
    }
}
