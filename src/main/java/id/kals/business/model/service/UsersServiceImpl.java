package id.kals.business.model.service;

import id.kals.business.model.entity.*;
import id.kals.business.model.enums.Status;
import id.kals.business.model.exceptions.DataNotFoundException;
import id.kals.business.model.model.ResultResponse;
import id.kals.business.model.repository.UsersRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
@Service
@Slf4j
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private PersonalService personalService;

    @Autowired
    private PersonalUserTypeService personalUserTypeService;

    @Override
    public Users findByIdAndStatus(String id, Status status) {
        return usersRepository.findByIdAndStatus(id, status);
    }

    @Override
    public Users findByUsernameAndStatus(String username, Status status) {
        return usersRepository.findByUsernameAndStatus(username, status);
    }

    @Override
    public Page<Users> findAllSpecification(Specification<Users> usersSpecification, Pageable pageable) {
        return usersRepository.findAll(usersSpecification, pageable);
    }

    @Override
    @Transactional
    public ResultResponse deleteUserRegistered(String username) {
        ResultResponse response = null;
        Users users = this.findByUsernameAndStatus(username, Status.ACTIVE);
        if (users != null) {
            this.delete(users);
            List<PersonalUserType> personalUserTypeList = personalUserTypeService.findByUserAndStatus(users, Status.ACTIVE);
            if (!personalUserTypeList.isEmpty()) {
                for (PersonalUserType personalUserType : personalUserTypeList) {
                    personalUserTypeService.delete(personalUserType);
                    Personal personal = personalUserType.getPersonal();
                    personalService.delete(personal);
                }
            }
            response = new ResultResponse();
            response.setCode("200");
            response.setContentId(users.getId());
            response.setDescription("Success Delete");
            response.setMessage("Success Delete");
        }else{
            throw new DataNotFoundException("Data Not Found with username: "+username);
        }
        return response;
    }

    @Override
    @Transactional
    public ResultResponse registerUser(Users users, Personal personal, CategoryType categoryType) {
        ResultResponse response = null;
        if (users != null && personal != null && categoryType != null) {
            Users usersSave = this.save(users);
            Personal personalSave = personalService.save(personal);


            PersonalUserType personalUserTypePre = new PersonalUserType();
            personalUserTypePre.setUser(usersSave);
            personalUserTypePre.setCategoryType(categoryType);
            personalUserTypePre.setPersonal(personalSave);
            personalUserTypePre.setStatus(Status.ACTIVE);
            personalUserTypePre.setCreateAt(LocalDateTime.now());
            personalUserTypePre.setCreateBy(personalSave.getCreateBy());
            personalUserTypePre.setCreateIp(personalSave.getCreateIp());
            PersonalUserType personalUserTypeSave = personalUserTypeService.save(personalUserTypePre);

            response = new ResultResponse();
            response.setCode("200");
            response.setContentId(users.getId());
            response.setDescription("Success Delete");
            response.setMessage("Success Delete");
        }

        return response;
    }

    @Override
    public Users save(Users entity) {
        Users response = new Users();

        if (!StringUtils.isEmpty(entity.getId())) {
            Users validUsers = usersRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validUsers != null) {
                BeanUtils.copyProperties(entity, validUsers);
                validUsers.setStatus(entity.getStatus());
                validUsers.setModifiedAt(entity.getModifiedAt());
                validUsers.setModifiedBy(entity.getModifiedBy());
                validUsers.setModifiedIp(entity.getModifiedIp());

                Users updateUsers = usersRepository.save(validUsers);
                response = updateUsers;
            }
        } else
            response = usersRepository.save(entity);

        return response;
    }

    @Override
    public Boolean delete(Users entity) {
        Boolean response = false;

        if (!StringUtils.isEmpty(entity.getId())) {
            Users validUsers = usersRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validUsers != null) {
                validUsers.setStatus(Status.DELETE);
                usersRepository.save(validUsers);

            }
        }
        return response;
    }

    @Override
    public Users findById(String id) {
        return usersRepository.findById(id);
    }

    @Override
    public Page<Users> findAllPage(Pageable pageable) {
        return usersRepository.findAll(pageable);
    }
}
