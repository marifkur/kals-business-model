package id.kals.business.model.service;

import id.kals.business.model.entity.Personal;
import id.kals.business.model.entity.PersonalReport;
import id.kals.business.model.entity.PersonalReportDetail;
import id.kals.business.model.enums.Status;
import id.kals.business.model.model.PersonalReportDetailModel;
import id.kals.business.model.model.PersonalReportWithDetailModel;
import id.kals.business.model.repository.PersonalReportRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Slf4j
@Service
public class PersonalReportServiceImpl implements PersonalReportService {

    @Autowired
    private PersonalReportRepository personalReportRepository;

    @Autowired
    private PersonalReportDetailService personalReportDetailService;

    @Override
    public PersonalReport findByIdAndStatus(String id, Status status) {
        return personalReportRepository.findByIdAndStatus(id, status);
    }

    @Override
    public List<PersonalReport> findByPersonalEmployeeAndStatus(Personal personal, Status status) {
        return personalReportRepository.findByPersonalEmployeeAndStatus(personal, status);
    }

    @Override
    public List<PersonalReport> findByPersonalVisitorAndStatus(Personal personal, Status status) {
        return personalReportRepository.findByPersonalVisitorAndStatus(personal, status);
    }

    @Override
    public List<PersonalReport> findByPersonalRespondentAndStatus(Personal personal, Status status) {
        return personalReportRepository.findByPersonalRespondentAndStatus(personal, status);
    }

    @Override
    public Page<PersonalReport> findAllSpecification(Specification<PersonalReport> personalReportSpecification, Pageable pageable) {
        return personalReportRepository.findAll(personalReportSpecification, pageable);
    }

    @Override
    @Transactional
    public PersonalReportWithDetailModel saveWithDetail(PersonalReport personalReport, List<PersonalReportDetailModel> personalReportDetailModels) {
        PersonalReportWithDetailModel personalReportWithDetailModel = new PersonalReportWithDetailModel();
        PersonalReport response = new PersonalReport();

        if (!StringUtils.isEmpty(personalReport.getId())) {
            PersonalReport validPersonalReport = personalReportRepository.findByIdAndStatus(personalReport.getId(), Status.ACTIVE);
            if (validPersonalReport != null) {
                BeanUtils.copyProperties(personalReport, validPersonalReport);
                validPersonalReport.setStatus(personalReport.getStatus());
                validPersonalReport.setModifiedAt(personalReport.getModifiedAt());
                validPersonalReport.setModifiedBy(personalReport.getModifiedBy());
                validPersonalReport.setModifiedIp(personalReport.getModifiedIp());
                validPersonalReport.setPersonalEmployee(personalReport.getPersonalEmployee());
                validPersonalReport.setPersonalVisitor(personalReport.getPersonalVisitor());
                validPersonalReport.setPersonalRespondent(personalReport.getPersonalRespondent());
                validPersonalReport.setStartDate(personalReport.getStartDate());
                validPersonalReport.setEndDate(personalReport.getEndDate());

                PersonalReport updatePersonalReport = personalReportRepository.save(validPersonalReport);
                response = updatePersonalReport;
            }
        } else
            response = personalReportRepository.save(personalReport);

        final PersonalReport personalReportFinal = response;
        List<PersonalReportDetail> personalReportDetailList = personalReportDetailModels.stream()
                .map(personalReportDetailModel -> {
                            PersonalReportDetail personalReportDetail = new PersonalReportDetail();
                            BeanUtils.copyProperties(personalReportDetailModel, personalReportDetail);
                            personalReportDetail.setPersonalReport(personalReportFinal);
                            personalReportDetail.setCost(personalReportDetailModel.getCost());
                            personalReportDetail.setCreateIp(personalReportFinal.getCreateIp());
                            personalReportDetail.setCreateBy(personalReportFinal.getCreateBy());
                            personalReportDetail.setCreateAt(personalReportFinal.getCreateAt());

                            PersonalReportDetail personalReportDetailSave = personalReportDetailService.save(personalReportDetail);
                            return personalReportDetailSave;
                        }
                ).collect(Collectors.toList());

        personalReportWithDetailModel.setPersonalReport(response);
        personalReportWithDetailModel.setPersonalReportDetailList(personalReportDetailList);

        return personalReportWithDetailModel;
    }

    @Override
    public PersonalReport save(PersonalReport entity) {
        PersonalReport response = new PersonalReport();

        if (!StringUtils.isEmpty(entity.getId())) {
            PersonalReport validPersonalReport = personalReportRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validPersonalReport != null) {
                BeanUtils.copyProperties(entity, validPersonalReport);
                validPersonalReport.setStatus(entity.getStatus());
                validPersonalReport.setModifiedAt(entity.getModifiedAt());
                validPersonalReport.setModifiedBy(entity.getModifiedBy());
                validPersonalReport.setModifiedIp(entity.getModifiedIp());
                validPersonalReport.setPersonalEmployee(entity.getPersonalEmployee());
                validPersonalReport.setPersonalVisitor(entity.getPersonalVisitor());
                validPersonalReport.setPersonalRespondent(entity.getPersonalRespondent());
                validPersonalReport.setStartDate(entity.getStartDate());
                validPersonalReport.setEndDate(entity.getEndDate());

                PersonalReport updatePersonalReport = personalReportRepository.save(validPersonalReport);
                response = updatePersonalReport;
            }
        } else
            response = personalReportRepository.save(entity);

        return response;
    }

    @Override
    public Boolean delete(PersonalReport entity) {
        Boolean response = false;

        if (!StringUtils.isEmpty(entity.getId())) {
            PersonalReport validPersonalReport = personalReportRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validPersonalReport != null) {
                validPersonalReport.setStatus(Status.DELETE);
                personalReportRepository.save(validPersonalReport);
                response = true;
            }
        }
        return response;
    }

    @Override
    public PersonalReport findById(String id) {
        return null;
    }

    @Override
    public Page<PersonalReport> findAllPage(Pageable pageable) {
        return null;
    }
}
