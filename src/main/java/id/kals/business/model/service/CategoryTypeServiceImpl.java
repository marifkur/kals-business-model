package id.kals.business.model.service;

import id.kals.business.model.entity.Category;
import id.kals.business.model.entity.CategoryType;
import id.kals.business.model.entity.Type;
import id.kals.business.model.enums.Status;
import id.kals.business.model.repository.CategoryTypeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Slf4j
@Service
public class CategoryTypeServiceImpl implements CategoryTypeService {
    @Autowired
    private CategoryTypeRepository categoryTypeRepository;

    @Override
    public CategoryType findByIdAndStatus(String id, Status status) {
        return categoryTypeRepository.findByIdAndStatus(id, status);
    }

    @Override
    public CategoryType findByCategoryAndStatus(Category category, Status status) {
        return categoryTypeRepository.findByCategoryAndStatus(category, status);
    }

    @Override
    public CategoryType findByTypeAndStatus(Type type, Status status) {
        return categoryTypeRepository.findByTypeAndStatus(type, status);
    }

    @Override
    public Page<CategoryType> findAllSpecification(Specification<CategoryType> categoryTypeSpecification, Pageable pageable) {
        return categoryTypeRepository.findAll(categoryTypeSpecification, pageable);
    }

    @Override
    public CategoryType save(CategoryType entity) {
        CategoryType response = new CategoryType();

        if (!StringUtils.isEmpty(entity.getId())) {
            CategoryType validCategoryType = categoryTypeRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validCategoryType != null) {
                BeanUtils.copyProperties(entity, validCategoryType);
                validCategoryType.setCategory(entity.getCategory());
                validCategoryType.setType(entity.getType());
                validCategoryType.setStatus(entity.getStatus());
                validCategoryType.setModifiedAt(entity.getModifiedAt());
                validCategoryType.setModifiedBy(entity.getModifiedBy());
                validCategoryType.setModifiedIp(entity.getModifiedIp());

                CategoryType updateCategoryType = categoryTypeRepository.save(validCategoryType);
                response = updateCategoryType;
            }
        } else
            response = categoryTypeRepository.save(entity);

        return response;
    }

    @Override
    public Boolean delete(CategoryType entity) {
        Boolean response = false;

        if (!StringUtils.isEmpty(entity.getId())) {
            CategoryType validCategoryType = categoryTypeRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validCategoryType != null) {
                validCategoryType.setStatus(Status.DELETE);
                categoryTypeRepository.save(validCategoryType);
                response = true;
            }
        }
        return response;
    }

    @Override
    public CategoryType findById(String id) {
        return categoryTypeRepository.findById(id);
    }

    @Override
    public Page<CategoryType> findAllPage(Pageable pageable) {
        return categoryTypeRepository.findAll(pageable);
    }
}
