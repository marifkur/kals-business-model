package id.kals.business.model.service;

import id.kals.business.model.entity.Personal;
import id.kals.business.model.enums.Status;
import id.kals.business.model.repository.PersonalRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/14/20
 */
@Slf4j
@Service
public class PersonalServiceImpl implements PersonalService {
    @Autowired
    private PersonalRepository personalRepository;

    @Override
    public Personal findByIdAndStatus(String id, Status status) {
        return personalRepository.findByIdAndStatus(id, status);
    }

    @Override
    public Page<Personal> findAllSpecification(Specification<Personal> personalSpecification, Pageable pageable) {
        return personalRepository.findAll(personalSpecification, pageable);
    }

    @Override
    public Personal save(Personal entity) {
        Personal response = new Personal();

        if (!StringUtils.isEmpty(entity.getId())) {
            Personal validPersonal = personalRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validPersonal != null) {
                BeanUtils.copyProperties(entity, validPersonal);
                validPersonal.setStatus(entity.getStatus());
                validPersonal.setModifiedAt(entity.getModifiedAt());
                validPersonal.setModifiedBy(entity.getModifiedBy());
                validPersonal.setModifiedIp(entity.getModifiedIp());
                validPersonal.setGender(entity.getGender());
                validPersonal.setMaritalStatus(entity.getMaritalStatus());
                validPersonal.setReligion(entity.getReligion());
                validPersonal.setBirthDate(entity.getBirthDate());

                Personal updatePersonal = personalRepository.save(validPersonal);
                response = updatePersonal;
            }
        } else
            response = personalRepository.save(entity);

        return response;
    }

    @Override
    public Boolean delete(Personal entity) {
        Boolean response = false;

        if (!StringUtils.isEmpty(entity.getId())) {
            Personal validPersonal = personalRepository.findByIdAndStatus(entity.getId(), Status.ACTIVE);
            if (validPersonal != null) {
                validPersonal.setStatus(Status.DELETE);
                personalRepository.save(validPersonal);
                response = true;
            }
        }
        return response;
    }

    @Override
    public Personal findById(String id) {
        return personalRepository.findById(id);
    }

    @Override
    public Page<Personal> findAllPage(Pageable pageable) {
        return personalRepository.findAll(pageable);
    }
}
