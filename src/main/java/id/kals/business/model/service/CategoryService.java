package id.kals.business.model.service;

import id.kals.business.model.entity.Category;
import id.kals.business.model.enums.Status;
import id.kals.business.model.utils.BaseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

/**
 * @Author arif <m.arif.kurniawan@myindo.co.id>
 * @Since 9/13/20
 */
public interface CategoryService extends BaseService<Category> {

    Category findByIdAndStatus(String id, Status status);

    List<Category> findByNameAndStatus(String name, Status status);

    Page<Category> findAllSpecification(Specification<Category> categorySpecification, Pageable pageable);

}